pokemon(bulbasaur,[grass,poison],49,49).
pokemon(weedle,[bug,poison],45,50).
pokemon(arbok,[posion],95,69).
pokemon(nidoqueen,[posion,ground],90,40).
pokemon(charmander,[fire],52,43).
pokemon(charmeleon,[fire],64,58).
pokemon(charizar,[fire,flying],84,78).
pokemon(vulpix,[fire],41,40).
pokemon(clefairy,[fairy],45,48).
pokemon(jigglypuff,[normal,fairy],45,20).
pokemon(squirtle,[water],48,65).
pokemon(caterpie,[bug],30,35).
pokemon(metapod,[bug],52,43).
pokemon(butterfree,[bug,flying],45,50).
pokemon(pikachu,[electric],55,40).
pokemon(sandshrew,[ground],90,40).



mostrarPokemones(L) :- 
	findall(pokemon(N,T,A,D),pokemon(N,T,A,D),L).

mostrarPokemonTipo([  ],L) :- nl.
mostrarPokemonTipo([C|Resto],L) :- 
	findall(pokemon(X1,X2,X3,X4),(pokemon(X1,X2,X3,X4),member(C,X2)),Li),
	append(Li,LiL,L),
	mostrarPokemonTipo(Resto,LiL).

calcularLista(X,Y):-mostrarPokemonTipo(X,Y).

eliminaTodos(X,Ys,Zs):-
	delete(Ys,X,Zs).

pokemonTipo(X,Y):-
	X=pokemon(_,T,_,_),
	mostrarPokemonTipo(T,Z),!,
	eliminaTodos(X,Z,Y),!.

/* Similar Caracteristica*/	

similar_caracteristica(P,L,'Ataque'):- 
	pokemonTipo(P,Result), similar_ataque(P,Result,L).

similar_caracteristica(P,L,'Defensa'):-
	pokemonTipo(P,Result), similar_defensa(P,Result,L).

similar_caracteristica(P,L,'AtaqueyDefensa'):-
	pokemonTipo(P,Result), similar_ataque_defensa(P,Result,L).

similar_ataque(_,[],[]):-!.
similar_ataque(pokemon(X1,X2,X3,X4),[pokemon(Y1,Y2,Y3,Y4)|R],[pokemon(Y1,Y2,Y3,Y4)|L]):-
	rangoAceptable(X3,Y3),
	not(rangoAceptable(X4,Y4)),
	similar_ataque(pokemon(X1,X2,X3,X4),R,L),!.


similar_ataque(pokemon(X1,X2,X3,X4),[pokemon(Y1,Y2,Y3,Y4)|R],L):-
	similar_ataque(pokemon(X1,X2,X3,X4),R,L).

similar_defensa(_,[],[]):-!.
similar_defensa(pokemon(X1,X2,X3,X4),[pokemon(Y1,Y2,Y3,Y4)|R],[pokemon(Y1,Y2,Y3,Y4)|L]):-
	rangoAceptable(X4,Y4),
	not(rangoAceptable(X3,Y3)),
	similar_defensa(pokemon(X1,X2,X3,X4),R,L),!.

similar_defensa(pokemon(X1,X2,X3,X4),[pokemon(Y1,Y2,Y3,Y4)|R],L):-
	similar_defensa(pokemon(X1,X2,X3,X4),R,L).

/*
Similar Ataque y Defensa
*/
similar_ataque_defensa(_,[],[]):-!.
similar_ataque_defensa(pokemon(X1,X2,X3,X4),[pokemon(Y1,Y2,Y3,Y4)|R],[pokemon(Y1,Y2,Y3,Y4)|L]):-
	rangoAceptable(X4,Y4),
	rangoAceptable(X3,Y3),
	similar_ataque_defensa(pokemon(X1,X2,X3,X4),R,L),!.

similar_ataque_defensa(pokemon(X1,X2,X3,X4),[pokemon(Y1,Y2,Y3,Y4)|R],L):-
	similar_ataque_defensa(pokemon(X1,X2,X3,X4),R,L).


rangoAceptable(X,Y):-
	A is X-10,
	B is X+10,
	between(A,B,Y).
